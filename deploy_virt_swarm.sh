#!/bin/bash
#
# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>
# 
#
# Set DRIVER to type of Virtualization to be used
# Reference:  https://docs.docker.com/machine/drivers/
DRIVER=virtualbox
# Set WORKERS to the number of desired Workers
WORKERS=5

echo "Initializing Swarm mode..."

# Create the first manager and initialize the swarm
docker-machine create --driver $DRIVER manager-1
docker-machine ssh manager-1 -- docker swarm init --advertise-addr $(docker-machine ip manager-1)

echo "Adding the nodes to the Swarm..."

MGRTOKEN=`docker-machine ssh manager-1 docker swarm join-token manager | grep token | awk '{ print $5 }'`
TOKEN=`docker-machine ssh manager-1 docker swarm join-token worker | grep token | awk '{ print $5 }'`

for i in 2 3 ; do
  docker-machine create --driver $DRIVER manager-$i;
  docker-machine ssh manager-$i \
    -- docker swarm join --token ${MGRTOKEN} $(docker-machine ip manager-1):2377;
done

# Create Docker Workers
for i in $(seq $WORKERS); do
  docker-machine create --driver $DRIVER worker-$i;
  docker-machine ssh worker-$i \
    -- docker swarm join --token ${TOKEN} $(docker-machine ip manager-1):2377;
done

# Setup any networks
echo "Creating networking..."

eval $(docker-machine env manager-1)

# Put any necessary docker networks here
docker network create  -d overlay --attachable consul 
docker network create  -d overlay --attachable traefik-nonprod_default

# Any additional commands to run post-creation on the nodes
# for i in 1 2 3 ; do
#  docker-machine ssh manager-$i "mkdir -p consul/data"
# done
